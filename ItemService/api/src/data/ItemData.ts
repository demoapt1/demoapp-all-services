import { getRepository } from "typeorm";
import { getManager } from "typeorm";
import {Item} from "../entity/Item";
import { ItemHistory } from "../entity/ItemHistory";

export class ItemData {

    private itemRepository = getRepository(Item);
    private itemHistoryRepository = getRepository(ItemHistory);

    async all(params?: any) {
        if (!params) {
            return this.itemRepository.find();
        } else {
            return this.itemRepository.find(params);
        }
    }

    async one(params?: any) {
        return this.itemRepository.findOne({ ItemId: params.ItemId });
    }

    async save(params?: any) {
        await getManager().transaction(async transactionalEntityManager => {
            // save Item data :-
            let items = await this.itemRepository.findOne({ ItemId: params.ItemId });
            if (!items) {
                items = await this.itemRepository.create({
                            ItemId: params.ItemId,
                            ItemName: params.ItemName,
                            quantity: params.quantity
                        });
            } else {
                if (params.Action == 'Add') {
                    items.quantity = Number(items.quantity) + Number(params.quantity);
                } else if(Number(items.quantity) >= Number(params.quantity)) {
                    items.quantity = Number(items.quantity) - Number(params.quantity);
                } else {
                    throw Error('Insufficient quantity for that item');
                }
            }
            await this.itemRepository.save(items);

            // save ItemHistory data :-
            let itemHist = await this.itemHistoryRepository.create({
                ItemID: params.ItemId,
                UserID: params.UserId,
                Quantity: params.quantity,
                Action: params.Action,
                DateOfAction: params.DateOfAction
            });

            await this.itemHistoryRepository.save(itemHist);
        });
    }

    async remove(params?: any) {
        let itemToRemove = await this.itemRepository.findOne(params);
        await this.itemRepository.remove(itemToRemove);
    }

}