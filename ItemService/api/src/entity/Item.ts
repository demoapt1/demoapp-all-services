import {Entity, PrimaryColumn, Column} from "typeorm";

@Entity()
export class Item {

    @PrimaryColumn()
    ItemId: string;

    @Column()
    ItemName: string;

    @Column()
    quantity: number;

}
