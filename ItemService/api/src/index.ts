import "reflect-metadata";
import {createConnection} from "typeorm";
import * as express from "express";
import * as bodyParser from "body-parser";
import {Request, Response} from "express";
import { ItemRoutes } from "./routes/ItemRoutes";
let PORT;

if( process.env.NODE_ENV == "PROD" ) {
	PORT = process.env.PORT;
	}else{
	PORT = "3002" ;
	}


createConnection().then(async connection => {

    // create express app
    const app = express();
    app.use(bodyParser.urlencoded());
    app.use(bodyParser.json());

    // register express routes from defined application routes
    ItemRoutes.forEach(route => {
        (app as any)[route.method](route.route, (req: Request, res: Response, next: Function) => {
            const result = (new (route.controller as any))[route.action](req, res, next);
            if (result instanceof Promise) {
                result.then(result => result !== null && result !== undefined ? res.send(result) : undefined);

            } else if (result !== null && result !== undefined) {
                res.json(result);
            }
        });
    });

    // setup express app here
    // ...

    // start express server
    app.listen(PORT);

    console.log("Express server has started on port "+PORT+". Open http://localhost:"+PORT+"/items to see results");

}).catch(error => console.log(error));
