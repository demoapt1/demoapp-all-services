import {Entity, PrimaryColumn, Column} from "typeorm";

@Entity()
export class order_transaction {

    @PrimaryColumn()
    order_id: string;

    @Column()
    itemname: string;

    @Column()
    quantity: number;

    @Column()
    order_status: number;

    @Column()
    user_id: string;

    @Column()
    expected_dod: Date;
}
