import {getRepository} from "typeorm";
import {order_transaction} from "../entity/order_transaction";

export class OrderData {
    private orderRepository = getRepository(order_transaction);

    async all(params?: any) {
        if (!params) {
            return this.orderRepository.find();
        } else {
            return this.orderRepository.find(params);
        }
    }

    async one(params?: any) {
        return this.orderRepository.findOne(params);
    }

    async save(params?: any) {
        let orders = await this.orderRepository.findOne({order_id: params.order_id });
        if (!orders) {
            orders =  await this.orderRepository.create({
                order_id: params.order_id,
                itemname: params.itemname,
                quantity: params.quantity,
                order_status: params.order_status,
                user_id: params.user_id,
                expected_dod: params.expected_dod
            });
        } else {
            orders.itemname = params.itemname;
            orders.quantity = params.quantity;
            orders.order_status = params.order_status;
            orders.user_id = params.user_id;
            orders.expected_dod = params.expected_dod;
        }
        return this.orderRepository.save(orders);
    }

    async remove(params?: any) {
        let orderToRemove = await this.orderRepository.findOne(params);
        await this.orderRepository.remove(orderToRemove);
    }
}