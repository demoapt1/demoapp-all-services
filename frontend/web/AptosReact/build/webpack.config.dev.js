const path = require("path");
const webpack = require("webpack");

module.exports = {
//  entry: "./src/index.js",
  entry:[
    'webpack-dev-server/client?http://0.0.0.0:8080',
    'webpack/hot/only-dev-server',
     "./src/index.js"
	 ],
  mode: "development",
  module: {
    rules: [
      {
        test: /\.(js|jsx)$/,
        exclude: /(node_modules|bower_components)/,
        loader: 'babel-loader',
        options: { presets: ['@babel/preset-env'] }
      },
      {
        test: /\.css$/,
        use: [ 'style-loader', 'css-loader' ]
      },
      {
        test: /\.scss$/,
        use: [
            "style-loader", // creates style nodes from JS strings
            "css-loader", // translates CSS into CommonJS
        ]
    },  { 
      test: /.(png|jpg|jpeg|gif|svg|woff|woff2|ttf|eot)$/,
      use: "url-loader?limit=100000"
  }
    ]
  },
  resolve: { extensions: ['*', '.js', '.jsx'] },
  output: {
    path: path.resolve(__dirname, "../dist/"),
    publicPath: "http://0.0.0.0:8080/dist/",
    filename: "bundle.js"
  },
  devServer: {
    contentBase: path.join(__dirname, "../src/public"),
    port: 8080,
    hotOnly: true,
//    historyApiFallback: true,
//            hot: true,
//            inline: true,
//            stats: true,
//            noInfo: true,
//            quiet: true,

//            stats: 'errors-only',
            disableHostCheck: true,
            host: '0.0.0.0',
  },
  plugins: [ new webpack.HotModuleReplacementPlugin() ]
};
