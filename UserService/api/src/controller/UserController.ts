import {NextFunction, Request, Response} from "express";
import { UserData } from "../data/UserData";

export class UserController {

    private userData = new UserData();

    async authenticate(request: Request, response: Response, next: NextFunction) {
        try {
            let user = await this.userData.one(request.body);
            if (!user) {
                response.json({ status: 1, error: 'no user found' });
            } else {
                response.json({ status: 0, users: user });
            }
        } catch (err) {
            console.log(err);
            response.json({ status: 1, error: 'failed to authenticate users' });
        }
    }

    async all(request: Request, response: Response, next: NextFunction) {
        try {
            let users = await this.userData.all(request.params);
            response.json({ status: 0, users: users });
        } catch (err) {
            console.log(err);
            response.json({ status: 1, error: 'failed to fetch users' });
        }
    }

    async one(request: Request, response: Response, next: NextFunction) {
        try {
            let user = await this.userData.one(request.params);
            response.json({ status: 0, users: user }); 
        } catch (err) {
            console.log(err);
            response.json({ status: 1, error: 'failed to fetch user'});
        }
    }

    async save(request: Request, response: Response, next: NextFunction) {
        try {
            let user = await this.userData.save(request.body);
            response.json({ status: 0, users: user }); 
        } catch (err) {
            console.log(err);
            response.json({ status: 1, error: 'failed to save user' });
        }
    }

    async remove(request: Request, response: Response, next: NextFunction) {
        try {
            await this.userData.remove(request.body);
            return response.json({ status: 0, message: 'user has been removed' });
        } catch (err) {
            console.log(err);
            return response.json({ status: 1, error: 'failed to removed user' });
        }
    }

}