import {UserController} from "../controller/UserController";

export const UserRoutes = [{
    method: "post",
    route: "/login",
    controller: UserController,
    action: "authenticate"
}, {
    method: "get",
    route: "/users",
    controller: UserController,
    action: "all"
}, {
    method: "get",
    route: "/users/:id",
    controller: UserController,
    action: "one"
}, {
    method: "post",
    route: "/users",
    controller: UserController,
    action: "save"
}, {
    method: "delete",
    route: "/users",
    controller: UserController,
    action: "remove"
}];